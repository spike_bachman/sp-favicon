# SP Favicon implementation

These are the instructions for implementing a Samaritan's Purse favicon for a project. Using this method gives you most of what is needed to display the Samaritan's Purse icon for a variety of browsers and devices, such as primary favicon, touch icon and tile icon. Example: iOS's 'Add to Homescreen' option for Safari pages.

If you want to read more about the ins-and-outs of favicon usage, try this CSS-Tricks article. <https://css-tricks.com/favicon-quiz/>

Or you can check out this tool. <https://realfavicongenerator.net/>

Place the following files in the root of your project:

- android-chrome-192x192.png
- android-chrome-256x256.png
- apple-touch-icon.png
- favicon-16x16.png
- favicon-32x32.png
- favicon.ico
- manifest.json
- mstile-150x150.png
- safari-pinned-tab.svg

And then place the following links and meta markup in the <head> of your index.html document:
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />
<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16" />
<link rel="manifest" href="/manifest.json" />
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#798034" />
<meta name="theme-color" content="#ffffff" />

That should be it. Cheers!
